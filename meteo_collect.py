#!/usr/bin/env python3

from dataclasses import dataclass, asdict, field

from datetime import datetime
from decimal import Decimal

import requests
from tabulate import tabulate

from dataclasses_carbon import send_to_carbon

STATION_IDS = ("09-station-meteo-la-salade", "42-station-meteo-parc-compans-cafarelli")


@dataclass
class WeatherReport:
    station_id: str = field().with_carbon_config(exclude=True)
    date: datetime
    temperature: Decimal
    humidity: int
    rain: Decimal

    def local_time(self):
        from dateutil import tz

        return self.date.astimezone(tz.gettz())


def weather_reports(station_id):
    records = requests.get(
        f"https://data.toulouse-metropole.fr/api/records/1.0/search/?dataset={station_id}&sort=record_timestamp"
    ).json()["records"]

    records.sort(key=lambda x: x["record_timestamp"])
    values = [r["fields"] for r in records]

    reports = []
    for value in values:
        reports.append(
            WeatherReport(
                station_id,
                datetime.fromisoformat(value["heure_utc"]),
                Decimal(value["temperature_en_degre_c"]),
                value["humidite"],
                Decimal(value["pluie"]),
            )
        )
    return reports


def print_reports(station_id, reports):
    name = " ".join(station_id.split("-")[3:]).capitalize()
    print(name)
    print("-" * len(name))
    viewable = []
    for report in reports:
        converted = asdict(report)
        del converted["station_id"]
        converted["date"] = report.local_time()
        viewable.append(converted)
    print(tabulate(viewable, headers="keys", tablefmt="presto"))
    print()


def send(root_prefix: str, report: WeatherReport):
    prefix = f"{root_prefix}.{report.station_id}"
    host = "belette"
    import dataclasses_carbon
    dump = dataclasses_carbon.dump_dataclass(report, dataclasses_carbon.DataclassCarbonDump(prefix))
    return dump
#    send_to_carbon(prefix, report, host)


if __name__ == "__main__":
    for station_id in STATION_IDS:
        print_reports(station_id, weather_reports(station_id))
